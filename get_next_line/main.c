#include "get_next_line.h"
#include <time.h>
#include <unistd.h>

int	main()
{
	char *line;

	sleep(1);
	while(get_next_line(0, &line)>0)
	{
		ft_putendl(line);
		free(line);
	}
	sleep(100);
}

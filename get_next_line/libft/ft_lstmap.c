/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akpenou <akpenou@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/28 09:05:08 by akpenou           #+#    #+#             */
/*   Updated: 2015/11/28 20:59:33 by akpenou          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list	*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem))
{
	t_list	*begin_list;
	t_list	*l_temp;
	t_list	*current_lst;

	l_temp = NULL;
	if (!lst || f == NULL)
		return (NULL);
	if (!lst->next)
		return (f(lst));
	l_temp = lst->next;
	current_lst = f(lst);
	begin_list = current_lst;
	while (l_temp)
	{
		current_lst->next = f(l_temp);
		current_lst = current_lst->next;
		l_temp = l_temp->next;
	}
	return (begin_list);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_math_pow.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akpenou <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/02 11:45:02 by akpenou           #+#    #+#             */
/*   Updated: 2015/12/02 12:14:46 by akpenou          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

double	ft_math_pow(double n, int power)
{
	if (power > 0)
		return (n * ft_math_pow(n, power - 1));
	if (power < 0)
		return (1 / ft_math_pow(n, -power));
	return (1);
}

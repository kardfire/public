/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsub.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akpenou <akpenou@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/25 11:15:00 by akpenou           #+#    #+#             */
/*   Updated: 2015/11/28 18:38:16 by akpenou          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strsub(char const *s, unsigned int start, size_t len)
{
	size_t		i;
	char		*str_cut;

	if (!s)
		return (NULL);
	i = 0;
	s = s + start;
	str_cut = (char*)malloc(sizeof(char) * (len + 1));
	if (str_cut)
	{
		return (ft_strncpy(str_cut, s, len));
	}
	return (NULL);
}

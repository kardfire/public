/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sqrt.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akpenou <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/02 10:20:43 by akpenou           #+#    #+#             */
/*   Updated: 2015/12/02 11:51:34 by akpenou          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

double	ft_math_sqrt(int nbr, unsigned int precision)
{
	double			n;
	double			x;
	unsigned int	i;

	i = 0;
	x = (double)nbr;
	n = (double)nbr;
	while (i < precision)
	{
		x = 0.5 * (x + (n / x));
		i++;
	}
	return (x);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_point_distance.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akpenou <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/02 12:03:56 by akpenou           #+#    #+#             */
/*   Updated: 2015/12/02 12:17:26 by akpenou          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

double	ft_point_distance(t_point p1, t_point p2)
{
	return (ft_math_sqrt(
			ft_math_pow((p2.x - p1.x), 2) + ft_math_pow((p2.y - p1.y), 2), 10));
}

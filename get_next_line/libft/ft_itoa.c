/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akpenou <akpenou@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/25 09:56:13 by akpenou           #+#    #+#             */
/*   Updated: 2015/12/05 09:02:08 by akpenou          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

#define ABC ft_strconst

static char	*ft_int_to_char(unsigned int n, int pow_max, char *str)
{
	int		i;

	i = 0;
	while (pow_max > 0)
	{
		str[i] = (n / pow_max) % 10 + '0';
		pow_max /= 10;
		i++;
	}
	str[i] = '\0';
	return (str);
}

static int	ft_power(int nbr, int power)
{
	if (power > 0)
		return (nbr * ft_power(nbr, power - 1));
	if (power < 0)
		return (nbr * ft_power(nbr, power + 1));
	return (1);
}

char		*ft_itoa(int n)
{
	unsigned int		nbr;
	int					len;
	char				signe;
	char				*nbr_str;

	len = 0;
	signe = '+';
	if (n == -2147483648 || n == 2147483647)
		return ((n < 0 ? ABC("-2147483648") : ABC("2147483647")));
	if (n < 0)
		signe = '-';
	nbr = (n < 0 ? (unsigned int)-n : (unsigned int)n);
	while (nbr > (unsigned int)ft_power(10, len + 1))
		len++;
	if (!(nbr_str = (char *)malloc(sizeof(char) * (len + 2))))
		return (NULL);
	if (signe == '-')
	{
		*nbr_str = signe;
		ft_int_to_char(nbr, ft_power(10, len), (nbr_str + 1));
		return (nbr_str);
	}
	return (ft_int_to_char(nbr, ft_power(10, len), (nbr_str)));
}

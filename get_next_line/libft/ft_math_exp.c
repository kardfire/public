/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_exp.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akpenou <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/02 11:06:57 by akpenou           #+#    #+#             */
/*   Updated: 2015/12/28 15:05:05 by akpenou          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

double	ft_math_exp(int nbr)
{
	double			exp;
	unsigned int	i;
	unsigned int	n;

	i = 0;
	exp = 2.71828182846;
	if (nbr < 0)
		n = (unsigned int)-nbr;
	else
		n = (unsigned int)nbr;
	while (i < (unsigned int)n)
	{
		exp *= exp;
		i++;
	}
	if (nbr < 0)
		return (1 / exp);
	return (exp);
}

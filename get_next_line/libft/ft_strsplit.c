/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akpenou <akpenou@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/25 11:27:00 by akpenou           #+#    #+#             */
/*   Updated: 2015/12/05 08:59:28 by akpenou          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	ft_nb_words(char const *s, char c)
{
	int	i;
	int	words;

	i = -1;
	words = 0;
	if (*s == '\0')
		return (0);
	while (s[i + 1] != '\0')
	{
		i++;
		if ((s[i] != c && s[i + 1] == c) || (s[i] != c && s[i + 1] == '\0'))
			words++;
	}
	return (words);
}

static char	*ft_cp_word(char const *s, char stop)
{
	int		i;
	int		len;
	char	*word;

	i = 0;
	len = 0;
	while (s[len] != '\0' && s[len] != stop)
		len++;
	if (!(word = (char *)malloc(sizeof(char) * len)))
		return (NULL);
	while (i < len)
	{
		word[i] = s[i];
		i++;
	}
	word[i] = '\0';
	return (word);
}

char		**ft_strsplit(char const *s, char c)
{
	unsigned int	nb_words;
	char			**item;
	int				i;

	if (!s)
		return (NULL);
	i = 0;
	nb_words = ft_nb_words(s, c);
	if (!(item = (char **)malloc(sizeof(char *) * (nb_words + 1))))
		return (NULL);
	if (item)
	{
		while (*s != '\0')
		{
			while (*s == c)
				s++;
			item[i] = ft_cp_word(s, c);
			while (*s != c && *s != '\0')
				s++;
			i++;
		}
	}
	item[nb_words] = NULL;
	return (item);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akpenou <akpenou@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/25 08:33:38 by akpenou           #+#    #+#             */
/*   Updated: 2015/12/05 08:57:12 by akpenou          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strdup(const char *s)
{
	int		len;
	int		i;
	char	*cpy_s;

	i = 0;
	len = ft_strlen(s);
	if (!(cpy_s = (char *)malloc(sizeof(*s) * (len + 1))))
		return (NULL);
	while (i < len)
	{
		cpy_s[i] = s[i];
		i++;
	}
	return (cpy_s);
}

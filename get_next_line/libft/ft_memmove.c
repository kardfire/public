/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akpenou <akpenou@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/25 11:15:00 by akpenou           #+#    #+#             */
/*   Updated: 2015/11/28 18:19:35 by akpenou          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memmove(void *dest, const void *src, size_t n)
{
	size_t			i;
	unsigned char	*src_cpy;
	unsigned char	*dest_cpy;

	i = 0;
	src_cpy = (unsigned char*)src;
	dest_cpy = (unsigned char*)dest;
	if (src_cpy == dest_cpy)
		return (dest);
	if (src_cpy > dest_cpy)
	{
		while (i < n)
		{
			dest_cpy[i] = src_cpy[i];
			i++;
		}
		return (dest);
	}
	while (n)
	{
		dest_cpy[n - 1] = src_cpy[n - 1];
		n--;
	}
	return (dest);
}

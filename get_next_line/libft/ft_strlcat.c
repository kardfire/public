/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akpenou <akpenou@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/25 11:15:00 by akpenou           #+#    #+#             */
/*   Updated: 2015/11/28 17:24:21 by akpenou          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t	ft_strlcat(char *dest, const char *src, size_t n)
{
	size_t		i;
	size_t		len_max;
	size_t		j;

	i = 0;
	j = 0;
	len_max = ft_strlen(dest) + ft_strlen(src);
	if (n < ft_strlen(dest))
		return (ft_strlen(src) + n);
	while (dest[i] != '\0')
		i++;
	while (i + j < n - 1)
	{
		if (src[j] != '\0')
			dest[i + j] = src[j];
		else
		{
			dest[i + j] = '\0';
		}
		j++;
	}
	dest[i + j] = '\0';
	return (len_max);
}
